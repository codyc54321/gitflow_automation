#!/bin/bash

# http://clubmate.fi/upgrade-to-bash-4-in-mac-os-x/
# to ensure you have newest bash to use variables...install and then PERMANENTLY UPGRADE HERE:
# http://apple.stackexchange.com/questions/55989/change-my-shell-to-a-different-bash-version-at-usr-local-bin-bash

# Bash dictionaries only work with version 4+. This script ensures we use bash v4...see links above

BASH_FULL_VERSION=$(echo $BASH_VERSION) # $BASH_VERSION == machine builtin var
BASH_VERSION=$(echo ${BASH_FULL_VERSION:0:1}) # get first character: 4.4.2v3.4 --> 4
if [[ $BASH_VERSION -lt 4 ]]; then
    brew unlink bash
    brew update && brew install bash
    sudo bash -c 'echo /usr/local/bin/bash >> /etc/shells'
    chsh -s /usr/local/bin/bash `logname` # logname gets your current username
    echo "Upgraded to bash version 4..."
    echo "..."
    echo "Now close your terminal(s) and re-open it so you can use bash v4"
    exit 1
    # if you have issue after 'brew unlink bash', run 'brew link bash'

    # if that install of bash v4 fails too:
    #     follow kv-bash example at http://stackoverflow.com/questions/14370133/is-there-a-way-to-create-key-value-pairs-in-bash-script
fi
