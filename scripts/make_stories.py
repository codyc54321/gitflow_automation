#!/usr/bin/env python

import sys

from gitflow.utils.interactivity import gather_required_input, pick_from_list
from gitflow.utils.general import create_branch_name, unpack_branch_name, call_sp
from gitflow.pivotal_tracker_webdriver_client import PivotalTrackerDriver

try:
    cwd, branch_name = sys.argv[1], sys.argv[2]
except:
    cwd = sys.argv[1]
    branch_name = None

stories = []

INFO = {'branch_exists': []}


while True:
    # defaults
    branch_exists, points = False, 3

    default_no = "(default = 'no')"
    default_yes = "(default = 'yes')"

    story_type = pick_from_list("the story type", ['feature', 'bug', 'chore', 'release'], 'feature')

    # pick branch
    if branch_name:
        branch_name = create_branch_name(branch_name, story_type)
        branch_response = raw_input("\nIs branch '{}' OK? (enter a new branch or leave blank to accept)\n".format(branch_name))
        if branch_response:
            branch_name = create_branch_name(branch_response, story_type)
        else:
            branch_exists = True
    else:
        branch_name_input = raw_input("Please enter a branch name:\n")
        branch_name = create_branch_name(branch_name_input, story_type)

    if not branch_exists:
        branch_exists = raw_input("Does this branch already exist? {}\n".format(default_no))

    if branch_exists == 'y':
        INFO['branch_exists'].append(True)
    else:
        INFO['branch_exists'].append(False)

    # pick name and description
    while True:
        unpacked_branch = unpack_branch_name(branch_name)
        story_name_input = raw_input("\nPlease enter the story name (say 'keep' to use '{}'):\n".format(unpacked_branch))
        if story_name_input == 'keep':
            story_name = unpacked_branch
            break
        elif story_name_input:
            story_name = story_name_input
            break
        else:
            continue

    print(story_name)

    description = gather_required_input("\nPlease enter the story description.\nFor new lines, enter '\\r\\n':\n")

    # TODO: this script doesn't support release yet
    if story_type == 'feature':
        points = raw_input("\nPlease enter the story points (1, 2, 3, 5, 8, or leave blank for the default, 3):\n")
    start_it_response = raw_input("\nDo you want to mark this story as 'started'?\n{0}:\n".format(default_yes))
    start_it = False if start_it_response == 'n' else True
    stories.append({
        'story_name': story_name, 'description': description,
        'points': points or 3, 'branch_name': branch_name,
        'story_type': story_type, 'start_it': start_it
    })

    response = raw_input("\nDo you want to add another story? {0}:\n".format(default_no))
    if response == 'y':
        continue

    break

if not stories:
    raise Exception("Make some stories")

driver = PivotalTrackerDriver()

for i, story in enumerate(stories):
    story_id = driver.make_story(**story)
    final_branch_name = branch_name + '-' + str(story_id)

    branch_existed = INFO['branch_exists'][i]
    if branch_existed:
        # checkout named branch from the existing branch
        call_sp('git checkout {}'.format(branch_name), **{'cwd': cwd})
        call_sp('git checkout -b {}'.format(final_branch_name), **{'cwd': cwd})
    else:
        call_sp('git checkout develop', **{'cwd': cwd})
        # call_sp('git pull upstream develop', **{'cwd': cwd})
        call_sp('git checkout -b {}'.format(final_branch_name), **{'cwd': cwd})
    print("The new branch is named {}".format(final_branch_name))

driver.driver.quit()

print("Remember to update your branch with 'git pull upstream develop'")
