#!/bin/bash

. ~/.bash_profile

dirty=$(git_dirty)
current_branch=$(get_current_branch)


if [[ ${dirty} = "false" ]]; then
  echo "git clean"
else
  echo "Please commit or stash your changes and try again"
  exit 1
fi


if [[ $current_branch =~ [0-9]{4,}$ ]]; then
    :
else
    echo "If you already have a Pivotal tracker ticket number enter it now:"
    read ticket_number
fi


if [ "$ticket_number" -eq "$ticket_number" ] 2>/dev/null; then
  current_branch=$current_branch-$ticket_number
  git co -b $current_branch
else
  :
fi


if [ "$(uname)" == "Darwin" ]; then
    # http://stackoverflow.com/questions/3504945/timeout-command-on-mac-os-x
    gtimeout 5 git push -u origin $current_branch
else
    timeout 5 git push -u origin $current_branch
fi

# TODO:
# this portion isn't working... the if [ $? = "124" ]; doesn't catch it even tho $? prints out as '124'
# list of exit statuses for timeout:
# https://www.gnu.org/software/coreutils/manual/html_node/timeout-invocation.html
# if [ $? = "124" ]; then
#     printf "\nCan't connect to git...\nExiting\n"
# fi

PROJECT_NAME=`basename $(pwd)`

python $GITFLOW_AUTOMATION_PATH/scripts/open_pull_request.py $current_branch $PROJECT_NAME
