#!/usr/bin/env python

import os, sys

from gitflow.utils.general import call_sp
from gitflow.utils.git_utils import delete_and_backup_branch, get_branches

cwd = os.getcwd()


branches = get_branches()
print(branches)


for b in branches:
    delete_and_backup_branch(b)
