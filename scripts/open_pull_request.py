#!/usr/bin/env python

import sys

from gitflow.config import HOMEDEPOT_BASE_URL
from gitflow.github_webdriver_client import GithubDriver
from gitflow.config import CREDENTIALS

head_branch = sys.argv[1]
project_name = sys.argv[2]

print("Opening pull request for {}: {}".format(project_name, head_branch))

driver = GithubDriver(CREDENTIALS['home_depot'], main_url=HOMEDEPOT_BASE_URL)
driver.open_PR(head_branch, project_name)
driver.driver.quit()
