#!/bin/bash
# this shebang assumes you have bashv4 installed. If not, use /bin/bash


current_branch="$(git branch | grep '\* ' | sed 's/^.*\( .*\)/\1/g')"

dltbranch() {
    git push origin --delete $1
    git push upstream --delete $1
    git branch -D $1
}

backup_branch() {
    git checkout $1
    git checkout -b backup__$1
    dltbranch $1
}

reset_branch() {
    the_new_current_branch="$(git branch | grep '\* ' | sed 's/^.*\( .*\)/\1/g')"
    if [ ! $the_new_current_branch = $1 ]; then
        git checkout $1
    fi
}

branch_is_protected(){
    if [[ "$1" == dev* || "$1" == "master" || "$1" == backup* ]]
    then
        echo "true"
        # exit 1
    else
        echo "false"
        # exit 0
    fi
}

backup_all_branches(){
    branches="$(git for-each-ref refs/heads | cut -d/ -f3-)"

    for branch in `echo "$branches"`; do
        is_protected="$(branch_is_protected $branch)"
        if [[ $is_protected = "false" ]]; then
            printf "\nBacking up branch: $branch\n\n"
            backup_branch "$branch";
        fi
    done
}


backup_all_branches

# reset the branch to whatever it was before running script
reset_branch $current_branch
