#!/bin/bash

. ~/.bash_profile

current_branch=$(get_current_branch)
echo $current_branch

cwd=$(pwd)

python $GITFLOW_AUTOMATION_PATH/scripts/make_stories.py $cwd $current_branch
