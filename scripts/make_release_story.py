#!/usr/bin/env python

import time

from gitflow.utils.general import process_comma_separated_list
from gitflow.pivotal_tracker_webdriver_client import PivotalTrackerDriver


pivotal_driver = PivotalTrackerDriver()

story_name = raw_input("\nPlease enter the story name:\n")
description = raw_input("\nPlease enter the story description.\nFor new lines, enter '\\n':\n")
banned_story_numbers = raw_input("\nPlease enter any banned story numbers to ignore (comma separated list):\n")
extra_story_numbers = raw_input("\nPlease enter any story numbers to add to the release (comma separated list):\n")


banned_story_numbers = process_comma_separated_list(banned_story_numbers)
extra_story_numbers = process_comma_separated_list(extra_story_numbers)

pivotal_driver.make_release_story(story_name, description, banned_story_numbers, extra_story_numbers)
