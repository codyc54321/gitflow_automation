## Add to .bashrc or .bash_profile:

    # github/pivotal tracker management
    GITFLOW_AUTOMATION_PATH="$PROJECTS/gitflow_automation"
    GITFLOW_AUTOMATION_SCRIPTS="$GITFLOW_AUTOMATION_PATH/scripts"

    export PYTHONPATH="${PYTHONPATH}:$GITFLOW_AUTOMATION_PATH"

    alias gitflowopenpullrequest="$GITFLOW_AUTOMATION_SCRIPTS/open_pull_request.sh"
    alias gitflowmakestory="$GITFLOW_AUTOMATION_SCRIPTS/make_stories.sh"
    alias gitflowmakereleasestory="$GITFLOW_AUTOMATION_SCRIPTS/make_release_story.py"
    alias gitflowbackupallbranches="$GITFLOW_AUTOMATION_SCRIPTS/backup_all_branches.sh"

So far supports the commands you see aliased above:

    open pull request
    make story
    make release story
    backup all branches


#### TODO:
 should use the pivotal api, not webdriver:

https://www.pivotaltracker.com/help/api/#Getting_Started

or pip:

https://pypi.python.org/pypi/pivotaltracker/0.0.3
https://pypi.python.org/pypi/busyflow.pivotal
https://github.com/iter8ve/pyvo

When making new stories, should also make a git branch afterward, with the new story number at the end like:
     feature/some-branch-name-14345367

When a pull request is made, the associated story should be set to 'finished'

If there's a merge conflict in a pull request, should email me saying which branch needs to be merged with dev

#### Docs:
PyGithub- http://pygithub.readthedocs.io/en/latest/introduction.html

#### Source:
https://github.com/PyGithub/PyGithub
