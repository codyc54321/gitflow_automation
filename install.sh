#!/bin/bash

brew install coreutils


if [ "$(uname)" == "Darwin" ]; then
    # http://stackoverflow.com/questions/3504945/timeout-command-on-mac-os-x
    brew install coreutils
else
    : # : means 'do nothing' in bash
fi

mkvirtualenv
