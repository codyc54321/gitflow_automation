
def wrap_in_quotes_if_string(item):
    if isinstance(item, str):
        item = "'" + item + "'"
    return item

def gather_required_input(message):
    while True:
        response = raw_input(message)
        if not response:
            continue
        return response

def generate_items_map_and_items_string(items):
    items_map = {}
    items_string = ""
    items_length = len(items)
    for i, item in enumerate(items):
        items_map[str(i + 1)] = item
        item = wrap_in_quotes_if_string(item)
        items_string += str(i + 1) + " - " + str(item) + "\n"
    return items_string, items_map

def generate_pick_from_list_message_and_items_map(message, items, default_item=None):

    if default_item:
        default_item = wrap_in_quotes_if_string(default_item)
        default_message = "(or leave blank for the default, {}):\n".format(default_item)
    else:
        default_message = ""

    items_string, items_map = generate_items_map_and_items_string(items)

    full_message = "\nPlease pick {message} by choosing a number:\n{items_string}{default_message}" \
                    .format(message=message, items_string=items_string, default_message=default_message)
    return full_message, items_map

def pick_from_list(message, items, default_item=None):
    full_message, items_map = generate_pick_from_list_message_and_items_map(message, items, default_item)
    while True:
        response = raw_input(full_message)
        if not response:
            if default_item:
                return default_item
            else:
                print("\nPlease pick an item\n")
                continue
        else:
            string_keys = [str(key) for key in items_map.keys()]
            if not str(response) in string_keys:
                print("\nPlease pick an item\n")
                continue
            else:
                return items_map[response]
