
from subprocess import Popen, PIPE

def process_comma_separated_list(the_input):
    if not the_input:
        return []
    unprocessed_list = the_input.split(",")
    return [item.strip() for item in unprocessed_list]

def create_branch_name(branch_name, story_type):
    branch_name = branch_name.replace(' ', '-')
    type_beginning = story_type + '/'
    if type_beginning not in branch_name:
        return type_beginning + branch_name
    return branch_name

def unpack_branch_name(branch):
    for branch_type in ['feature', 'bug', 'chore']:
        branch = branch.replace((branch_type + '/'), '')
    return branch.replace('_', ' ').replace('-', ' ')

def call_sp(command, *args, **kwargs):
    if args:
        command = command.format(*args)
    p = Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, **kwargs)
    output, err = p.communicate()
    print('\nOutput:\n')
    print(output.decode('utf-8'))
    print('\n\nErr:\n')
    print(err.decode('utf-8'))
    return output, err
