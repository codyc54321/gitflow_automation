from gitflow.utils.general import call_sp


BANNED_BRANCHES = ['develop', 'master']

def get_branches(cwd=None):
    kwargs = {'cwd': cwd} if cwd else {}
    output, error = call_sp('git branch', **kwargs)
    raw_branches = output.split('\n')
    branches_without_current_branch_star = [branch.replace('* ', '') for branch in raw_branches if branch] # the current branch shows like '* develop'
    stripped_branches = [branch.strip() for branch in branches_without_current_branch_star]
    branches = [branch.strip() for branch in stripped_branches if branch not in BANNED_BRANCHES]
    return branches

def backup_a_branch(branch_name, cwd=None):
    kwargs = {'cwd': cwd} if cwd else {}
    backup_branch_name = "backup_" + branch_name
    print(backup_branch_name)
    call_sp('git checkout {}'.format(branch_name), **kwargs)
    call_sp('git checkout -b {}'.format(backup_branch_name), **kwargs)


def delete_a_branch(branch_name, cwd=None):
    kwargs = {'cwd': cwd} if cwd else {}
    call_sp('git push origin --delete {}'.format(branch_name), **kwargs)
    call_sp('git branch -D {}'.format(branch_name), **kwargs)


def delete_and_backup_branch(branch_name, cwd):
    backup_a_branch(branch_name, cwd)
    delete_a_branch(branch_name, cwd)
