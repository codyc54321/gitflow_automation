import os
from time import sleep

from gitflow.config import PIVOTAL_BASE_URL, WORK_EMAIL, WORK_USERNAME
from gitflow.webdriver_base import FirefoxDriver, ChromeDriver
from gitflow.utils.general import create_branch_name, unpack_branch_name

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


MYWORK_STORIES_XPATH = """
//div[@id='panel_my_work_{project_id}']\
//section\
//div[contains(concat(' ', normalize-space(@class), ' '), ' panel_content ')]\
//div[contains(concat(' ', normalize-space(@class), ' '), ' story ') \
and contains(concat(' ', normalize-space(@class), ' '), ' finished ')]\
"""


class PivotalTrackerDriver(object):

    def __init__(self):
        self.username = WORK_EMAIL
        self.password = os.environ.get('WORK_PW')
        self.main_url  = PIVOTAL_BASE_URL
        self.login_url = self.main_url.rstrip('/') + "/signin"
        self.driver = ChromeDriver()
        self.project_id = "1862817"

    def make_release_story(self, story_name, description,
            banned_story_numbers=[None], extra_story_numbers=[], project_id="1862817"):
        self.ensure_logged_in()
        self.go_to_project(project_id)
        self.driver.wait("add_story")
        mywork_story_numbers = self.get_currently_finished_mywork_story_numbers(banned_story_numbers, project_id)
        mywork_story_numbers.extend(extra_story_numbers)
        story_numbers_as_tasks = map(lambda x: "#" + str(x), mywork_story_numbers)
        self.make_story(
            story_name=story_name,
            description=description,
            story_type='release',
            tasks=story_numbers_as_tasks
        )
        # TODO: print out the story titles to be closed, ask user if those are OK
        self.accept_all_finished_tasks(banned_story_numbers)

    # def accept_all_finished_tasks(self, banned_story_numbers=[]):
    #     mywork_story_elements = self.get_currently_finished_mywork_story_numbers(banned_story_numbers)
    #     for story_element in mywork_story_elements:
    #         element = story_element.find_element_by_xpath(".//label[text()='Deliver']")
    #         element.click()
    #         time.sleep(1.5)
    #         element = story_element.find_element_by_xpath(".//label[text()='Accept']")
    #         element.click()
    #         time.sleep(1.5)

    def accept_all_finished_tasks(self, banned_story_numbers, project_id=None):
        mywork_story_elements = self.driver.find_elements_by_xpath(MYWORK_STORIES_XPATH.format(project_id=project_id or self.project_id))
        for story_element in mywork_story_elements:
            print("there is a story_element!")
            story_number = self.get_story_number_from_story_div(story_element)
            if self.check_for_next_story_state(story_element, expected_state='deliver'):
                print("passed check_for_next_story_state()")
                if story_number not in banned_story_numbers:
                    print(story_number)
                    element = story_element.find_element_by_xpath(".//label[text()='Deliver']")
                    element.click()
                    sleep(1.5)
                    element = story_element.find_element_by_xpath(".//label[text()='Accept']")
                    element.click()
                    sleep(1.5)

    def get_currently_finished_mywork_story_numbers(self, banned_story_numbers=[], project_id=None):
        mywork_story_elements = self.driver.find_elements_by_xpath(MYWORK_STORIES_XPATH.format(project_id=project_id or self.project_id))
        mywork_story_numbers = []
        for story_element in mywork_story_elements:
            story_number = self.get_story_number_from_story_div(story_element)
            if self.check_for_next_story_state(story_element, expected_state='deliver'):
                if story_number not in banned_story_numbers:
                    mywork_story_numbers.append(story_number)
        return mywork_story_numbers

    def get_story_number_from_story_div(self, story_div_element):
        classes = story_div_element.get_attribute("class").split(' ')
        for css_class in classes:
            if 'story_' in css_class:
                return css_class.replace('story_', '')

    def check_for_next_story_state(self, story_div_element, expected_state="deliver"):
        """
        the next state is where the story can go; finished stories show "Deliver", etc
        """
        this_xpath = "//header//span[@class='state']//label[@data-destination-state='{0}']".format(expected_state)
        try:
            element = story_div_element.find_element_by_xpath(this_xpath)
            return bool(element)
        except:
            return None

    def make_story(self, story_name=None, description=None, branch_name=None, story_type="feature", points=3,
            start_it=False, project_id="1862817", tasks=[]):
        if not story_name or not description:
            raise Exception("Each story needs a name and description")

        self.go_to_project(project_id)
        self.driver.click_button("Add Story")
        self.driver.find_box_and_fill(story_name, "story[name]")
        self.pick_story_type(story_type)

        # story points
        if story_type == 'feature':
            self.pick_story_points(points)

        # get child elements of any depth:
        # http://stackoverflow.com/questions/16016521/how-do-i-select-child-elements-of-any-depth-using-xpath
        # find by CSS class:
        # http://stackoverflow.com/questions/1604471/how-can-i-find-an-element-by-css-class-with-xpath
        xpath = "//div[contains(concat(' ', normalize-space(@class), ' '), ' add_owner ')]//span[normalize-space(text())='Childers, Cody K']"
        self.driver.click_button("add_owner")
        element = self.driver.find_element_by_xpath(xpath)
        element.click()

        # fill description
        self.driver.click_button("(edit)")

        # self.driver.find_box_and_fill(description, "story[pending_description]")
        element = self.driver.find_element_by_xpath("//div[contains(@class, 'DescriptionEdit_')]/textarea")
        element.send_keys(description)

        self.driver.click_button("Done")

        # label as the project
        self.driver.click_button("Add a label")
        self.driver.active_element.send_keys("siteops dashboard")
        self.driver.click_active_by_hitting_enter()

        self.add_tasks(tasks)

        sleep(1)
        self.driver.click_button("Save")
        sleep(3)

        story_id = self.get_story_id(story_name)

        if start_it:
            # TODO: filter label by "Start" text like label[text()='Start'] or whatnot
            start_button_xpath = "//span[span[@class='story_name' and text()='{story_name}']]\
                                  /preceding-sibling::span/label".format(story_name=story_name)
            element = self.driver.find_element_by_xpath(start_button_xpath)
            self.driver.click_button(xpath=start_button_xpath)

        sleep(2)

        return story_id

    def pick_story_type(self, story_type):
        if story_type != 'feature':
            assert story_type in ['bug', 'chore', 'release']
            self.driver.click_button('item_feature')
            self.driver.click_button('item_{0}'.format(story_type))
            # releases need a date, which is today since we're opening the story today
            if story_type == 'release':
                self.driver.click_button('calendar_icon')
                sleep(1)
                self.driver.click_active_by_hitting_enter()
                sleep(1)

    def pick_story_points(self, points):
        self.driver.click_button("unestimated")
        self.driver.click_button("item_" + str(points))

    def add_tasks(self, tasks):
        self.driver.click_button("Add a task")
        if tasks:
            for task in tasks:
                self.driver.active_element.send_keys(task)
                self.driver.click_active_by_hitting_enter()
        else:
            self.driver.active_element.send_keys("code")
            self.driver.click_active_by_hitting_enter()
            self.driver.active_element.send_keys("open PR")
            self.driver.click_active_by_hitting_enter()
            self.driver.active_element.send_keys("merge PR")
            self.driver.click_active_by_hitting_enter()

    def go_to_project(self, project_id="1862817"):
        self.ensure_logged_in()
        URL_END = "/n/projects/" + project_id
        URL = self.main_url + URL_END
        if self.driver.current_url != URL:
            self.driver.get(URL)
        self.driver.wait("add_story")
        sleep(2)

    def login(self):
        self.driver.get(self.login_url)
        sleep(2)
        self.driver.find_box_and_fill(search_text="credentials_username", value=self.username)
        self.driver.click_button("NEXT")
        self.driver.wait("j_username")
        self.driver.find_box_and_fill(search_text="j_username", value="ckc3153")
        self.driver.find_box_and_fill(search_text="j_password", value=self.password)
        self.driver.click_button("Sign In")
        sleep(4)

    def ensure_logged_in(self):
        if not self.is_logged_in:
            self.login()

    @property
    def is_logged_in(self):
        contents = str(self.driver.soup.encode_contents())
        return "Childers, Cody K" in contents or "childersc" in contents

    def get_story_id(self, story_name):
        element = self.get_story_div_element(story_name)
        return element.get_attribute('data-id')

    def get_story_div_element(self, story_name):
        story_div_xpath = "//div[./header/span/span[@class='story_name' and text()='{story_name}']]".format(story_name=story_name)
        return self.driver.find_element_by_xpath(story_div_xpath)
