import time

from gitflow.config import HOMEDEPOT_BASE_URL, WORK_USERNAME
from gitflow.webdriver_base import ChromeDriver, FirefoxDriver


class GithubDriver(object):

    def __init__(self, credentials, main_url="https://github.com"):
        self.username = credentials['username']
        self.password = credentials['password']
        self.main_url  = main_url
        self.login_url = main_url.rstrip('/') + "/login"
        # self.driver = ChromeDriver()
        self.driver = FirefoxDriver()

    def login(self):
        self.driver.get(self.login_url)
        time.sleep(2)
        self.driver.find_box_and_fill(search_text="login_field", value=self.username)
        self.driver.find_box_and_fill(search_text="password", value=self.password)
        self.driver.click_button("Sign in")
        time.sleep(2)

    def ensure_logged_in(self):
        if not self.is_logged_in:
            self.login()

    @property
    def is_logged_in(self):
        return "Signed in as" in self.driver.soup.encode_contents()

    def open_PR(
        self,
        head_branch=None,
        project_name="metricsportal",
        message="",
        username=WORK_USERNAME,
        assignees=[
            'not working', # Venu
            'the webdriver portion here isnt working', # Nathan
            'not working' # Shawn
        ]
    ):

        self.ensure_logged_in()

        FOUND_THE_URL = False
        for group in ['Rehub', 'perfeng']:
            if not FOUND_THE_URL:
                try:
                    URL_END = "/{group}/{project_name}/compare/develop...{username}:{head_branch}?expand=1".format(
                            group=group, project_name=project_name, username=username, head_branch=head_branch
                    )
                    URL = HOMEDEPOT_BASE_URL + URL_END
                    self.driver.get(URL)
                    if not self.driver.text_is_on_page('not the web page you are looking for'):
                        FOUND_THE_URL = True
                except:
                    print("Exception for %s" % URL_END)

        # the apostrophe in isn't is actually a smart quote, so it won't find it
        if self.driver.text_is_on_page("There isn") and self.driver.text_is_on_page("anything to compare"):
            URL_END = "/{group}/{project_name}/compare/develop...{head_branch}?expand=1".format(
                    group=group, project_name=project_name, username=username, head_branch=head_branch
            )
            URL = HOMEDEPOT_BASE_URL + URL_END
            self.driver.get(URL)
            time.sleep(2)
        assignees_button = self.driver.find_element_by_xpath('//button[contains(.,"Assignees")]')
        assignees_button.click()

        # TODO: fix the fact that it won't click usernames anymore
        # for username in assignees:
        #     self.pick_assignee(username)

        # time.sleep(0.3)
        # assignees_button.click()
        # self.driver.find_box_and_fill(value=message, search_text="pull_request_body")
        # self.driver.click_button("Create pull request")
        # time.sleep(2)

        # TODO: remove extra time for doing manually once script is fixed
        time.sleep(60)
                # except:
                #     pass

        self.driver.quit()

    def pick_assignee(self, username):
        xpath = "//span[normalize-space(text())='{username}']/ancestor::div[contains(concat(' ', @class, ' '), ' js-navigation-item ')]" \
                .format(username=username)
        element = self.driver.find_element_by_xpath(xpath)
        self.driver.execute_script("arguments[0].setAttribute('class', 'select-menu-item js-navigation-item selected')", element)
        # self.driver.click_button(xpath=xpath)
