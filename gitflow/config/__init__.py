import os

WORK_EMAIL = os.environ.get("WORK_EMAIL")
WORK_USERNAME = os.environ.get("WORK_USERNAME")
HOMEDEPOT_BASE_URL = "https://github.homedepot.com"
PIVOTAL_BASE_URL = "https://www.pivotaltracker.com"

CREDENTIALS = {
    'normal': {'username': 'codyc4321', 'password': os.environ.get("THE_USUAL")},
    'home_depot': {'username': WORK_USERNAME, 'password': os.environ.get("WORK_PW")},
}
