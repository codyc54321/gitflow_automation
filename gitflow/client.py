# import os
from github import Github


class GithubClient(object):

    def __init__(self, credentials, **kwargs):
        self.user = Github(credentials['username'], credentials['password'], **kwargs)

    @property
    def repos(self):
        try:
            return [repo.name for repo in self.user.get_user().get_repos()]
        except:
            return []
