
from unittest import TestCase

from gitflow.utils.general import create_branch_name, unpack_branch_name


class GeneralUtilsTests(TestCase):

    def test_create_branch_name_combines_story_type(self):
        result = create_branch_name('my-Awesome_Branch name', 'chore')
        self.assertEqual(result, 'chore/my-Awesome_Branch-name')

    def test_create_branch_name_ignores_story_type(self):
        result = create_branch_name('chore/my-Awesome_Branch name', 'chore')
        self.assertEqual(result, 'chore/my-Awesome_Branch-name')

    def test_unpack_branch_name(self):
        result = unpack_branch_name('chore/my-Awesome_Branch-name')
        self.assertEqual(result, 'my Awesome Branch name')
