import mock
from unittest import TestCase

from gitflow.utils.interactivity import (
    wrap_in_quotes_if_string,
    gather_required_input,
    generate_items_map_and_items_string,
    generate_pick_from_list_message_and_items_map,
    pick_from_list,
)


class InteractivityUtilsTests(TestCase):

    def test_wrap_in_quotes_if_string_wraps_string(self):
        result = wrap_in_quotes_if_string("this thing")
        self.assertEqual(result, "'this thing'")

    def test_wrap_in_quotes_if_string_ignores_nonstring(self):
        result = wrap_in_quotes_if_string(3.23)
        self.assertEqual(result, 3.23)

    # side_effect runs through multiple mock calls, return_value returns one thing
    # here, side effect causes raw_input to be called 4 times, returning '', '', '', and 'something'
    # http://stackoverflow.com/questions/35774094/mock-python-function-with-multiple-return-values
    @mock.patch('gitflow.utils.interactivity.raw_input', side_effect=['', '', '', 'something'])
    def test_gather_required_input(self, mock_raw_input):
        result = gather_required_input('blah blah')
        self.assertEqual(result, 'something')

    def test_generate_items_map_and_items_string(self):
        items_string, items_map = generate_items_map_and_items_string(['one', 'two', 3, 4.4])
        self.assertEqual(items_map, {'1': 'one', '2': 'two', '3': 3, '4': 4.4})
        self.assertEqual(items_string, "1 - 'one'\n2 - 'two'\n3 - 3\n4 - 4.4\n")

    def test_generate_pick_from_list_message_and_items_map(self):
        full_message, items_map = generate_pick_from_list_message_and_items_map(
            message="from some items",
            items=['one', 2, 'three', 4.4],
            default_item=2
        )
        message_expectation = ("\nPlease pick from some items by choosing a number:\n" 
                              "1 - 'one'\n2 - 2\n3 - 'three'\n4 - 4.4\n(or leave blank for the default, 2):\n")
        self.assertEqual(full_message, message_expectation)
        self.assertEqual(items_map, {'1': 'one', '2': 2, '3': 'three', '4': 4.4})

    @mock.patch('gitflow.utils.interactivity.raw_input', return_value='1')
    def test_generate_pick_from_list_message_and_items_map_picks_correct_item(self, mock_raw_input):
        result = pick_from_list(
            message="from some items",
            items=['one', 2, 'three', 4.4],
            default_item=2
        )
        self.assertEqual(result, 'one')

    @mock.patch('gitflow.utils.interactivity.raw_input', return_value='')
    def test_generate_pick_from_list_message_and_items_map_picks_default(self, mock_raw_input):
        result = pick_from_list(
            message="from some items",
            items=['one', 2, 'three', 4.4],
            default_item=2
        )
        self.assertEqual(result, 2)
