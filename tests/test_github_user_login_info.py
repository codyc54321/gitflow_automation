from unittest import TestCase

from gitflow.config import CREDENTIALS
from client import GithubClient


class UserTests(TestCase):

    def test_personal_user(self):
        client = GithubClient(credentials=CREDENTIALS['normal'])
        self.assertTrue(client.repos)

    # def test_work_user(self):
    #     # kwargs = CREDENTIALS['home_depot']
    #     # kwargs['base_url'] = 'https://api.github.homedepot.com'
    #     client = GithubClient(credentials=CREDENTIALS['home_depot'], base_url='https://api.github.homedepot.com')
    #     self.assertTrue(client.repos)
