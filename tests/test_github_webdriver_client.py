from unittest import TestCase, skip

from gitflow.config import HOMEDEPOT_BASE_URL
from gitflow.github_webdriver_client import GithubDriver
from gitflow.config import CREDENTIALS


class GithubDriverTests(TestCase):

    def tearDown(self):
        self.webdriver.driver.quit()

    @skip('slow test')
    def test_login_normal(self):
        self.webdriver = GithubDriver(CREDENTIALS['normal'])
        self.webdriver.login()
        self.assertTrue(self.webdriver.is_logged_in)

    @skip('slow test')
    def test_login_work(self):
        self.webdriver = GithubDriver(CREDENTIALS['home_depot'], main_url=HOMEDEPOT_BASE_URL)
        self.webdriver.login()
        self.assertTrue("Signed in as" in self.webdriver.driver.soup.encode_contents())
