from unittest import TestCase, skip
from time import sleep

from gitflow.pivotal_tracker_webdriver_client import PivotalTrackerDriver


class PivotalTrackerDriverTests(TestCase):

    def setUp(self):
        self.webdriver = PivotalTrackerDriver()

    def tearDown(self):
        try:
            self.webdriver.driver.quit()
        except:
            pass

    # @skip("slow test")
    def test_make_story(self):
        story_name = "test"
        self.webdriver.make_story(
            story_name=story_name, description="test story",
            branch_name="test-branch", story_type="feature",
            points=5, start_it=True
        )
        sleep(2)
        self.assertTrue(self.webdriver.get_story_div_element(story_name))
        # TODO: clean up the stories, delete them

    @skip("slow test")
    def test_go_to_project(self):
        self.webdriver.login()
        self.webdriver.go_to_project()
        sleep(2)
        self.assertTrue(self.webdriver.driver.text_is_on_page("current iteration"))

    @skip("slow test")
    def test_login(self):
        # self.webdriver = PivotalTrackerDriver()
        self.webdriver.login()
        sleep(2)
        self.assertTrue(self.webdriver.is_logged_in)

    # def test_make_story(self):
    #     self.webdriver = PivotalTrackerDriver()
    #     self.webdriver.make_story(
    #         "standardize bq calls",
    #         "feature/standardize-bq-calls   This feature mainly related to order_metrics_utils; standardize 1/7 day prior, and bq execution flow",
    #         points=5
    #     )

    # def test_make_stories(self):
    #     self.webdriver = PivotalTrackerDriver()
    #     self.webdriver.make_story(
    #         "standardize bq calls",
    #         "feature/standardize-bq-calls   This feature mainly related to order_metrics_utils; standardize 1/7 day prior, and bq execution flow",
    #         points=5
    #     )
